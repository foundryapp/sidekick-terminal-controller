import { RemoteConnection } from './remoteConnection';
import { SidekickConnection } from './sidekickConnection';
import { Terminal } from './terminal';


class Session {
  private remote?: RemoteConnection;
  private terminal: Terminal;
  private sidekick: SidekickConnection;

  private terminalOnData?: any;
  private stdinOnData?: any;

  private isHost = true;

  getIsHost() {
    return this.isHost;
  }

  constructor(private forkCount: number) {
    this.terminal = new Terminal(this.forkCount);

    const info = {
      pid: this.terminal.pid,
      cwd: process.env.PWD,
      cols: this.terminal.cols,
      rows: this.terminal.rows,
    };

    process.on('SIGWINCH', () => {
      if (this.getIsHost()) {
        this.terminal.adjustSize();
      }
    });

    this.setHostIORedirect();

    this.sidekick = new SidekickConnection(info);

    this.sidekick.controlEmitter.on('create_peer_session', (sessionId: string) =>
      this.handleCreatePeerSession(sessionId)
    );

    this.sidekick.controlEmitter.on('join_peer_session', (sessionId: string) =>
      this.handleJoinPeerSession(sessionId)
    );

    this.sidekick.controlEmitter.on('exit_peer_session', () =>
      this.handleExitPeerSession()
    );

    this.sidekick.controlEmitter.on('awaiting', () => {
      this.terminal.turnHighlightOn('Please wait', 'white');
    });

    this.sidekick.controlEmitter.on('highlight_on', (message: string, color: string) => {
      this.terminal.turnHighlightOn(message, color);
    });

    this.sidekick.controlEmitter.on('highlight_off', () => {
      this.terminal.turnHighlightOff();
    });
  }

  private async handleCreatePeerSession(sessionId: string) {
    this.terminal.turnHighlightOff();

    this.remote?.close();

    this.remote = new RemoteConnection(sessionId);
    await this.remote.initialize();
    await this.remote.createPeerSession();

    this.remote.controlEmitter.once('close', () => {
      this.handleExitPeerSession();
    });

    this.remote.controlEmitter.once('connect', () => {
      this.terminal.turnHighlightOff();
    });

    process.stdout.write('\rSession created\n');

    this.setHostIORedirect();
  }

  private async handleJoinPeerSession(sessionId: string) {
    this.terminal.turnHighlightOff();
    this.remote?.close();

    this.remote = new RemoteConnection(sessionId);

    await this.remote.initialize();
    await this.remote.joinPeerSession();

    this.remote.controlEmitter.once('close', () => {
      console.log('close, redirecting');
      process.stdout.write('\nSession ended\n');
      this.handleExitPeerSession();
    });

    process.stdout.write('\nSession joined\n');

    this.setClientIORedirect();
    this.terminal.refresh();
  }

  private async handleExitPeerSession() {
    this.terminal.turnHighlightOff();
    this.remote?.close();
    this.remote = undefined;

    process.stdout.write('\nSession ended\n');
    this.setHostIORedirect();
  }

  async connect() {
    await this.sidekick.connect();
  }

  setClientIORedirect() {
    this.cleanIORedirect();

    this.isHost = false;

    const stdinOnData = (data: string) => this.stdinOnDataHandler(data, false);
    this.stdinOnData = stdinOnData;
    process.stdin.on('data', stdinOnData);


    this.remote?.dataEmitter.on('stdin', (data) =>
      this.terminal?.write(data)
    );

    this.remote?.dataEmitter.on('stdout', (data) =>
      process.stdout.write(data)
    );

    this.remote?.dataEmitter.on('stderr', (data) =>
      process.stderr.write(data)
    );

    this.remote?.dataEmitter.on('resize', (data) =>
      this.terminal?.setSize(data.cols, data.rows)
    );
  }

  setHostIORedirect() {
    this.cleanIORedirect();

    this.isHost = true;

    const stdinOnData = (data: string) => this.stdinOnDataHandler(data, true);
    this.stdinOnData = stdinOnData;
    process.stdin.on('data', stdinOnData);

    this.terminalOnData = this.terminal.onData((data) => {
      this.remote?.sendData('stdout', data);
      process.stdout.write(data);
    });

    this.remote?.dataEmitter.on('stdin', (data) =>
      this.terminal?.write(data)
    );

    this.remote?.dataEmitter.on('stdout', (data) =>
      process.stdout.write(data)
    );

    this.remote?.dataEmitter.on('stderr', (data) =>
      process.stderr.write(data)
    );

    this.remote?.dataEmitter.on('resize', (data) =>
      this.terminal?.setSize(data.cols, data.rows)
    );
  }

  cleanIORedirect() {
    if (this.stdinOnData) {
      process.stdin.removeListener('data', this.stdinOnData);
    }
    this.terminalOnData?.dispose();
    this.remote?.dataEmitter.removeAllListeners();
  }

  stdinOnDataHandler(data: string, isHost: boolean) {
    return isHost
      ? this.terminal.write(data.toString())
      : this.remote?.sendData('stdin', data.toString())
  }
}


export { Session };
