import io from 'socket.io-client';
import { EventEmitter } from 'events';

const terminalsPort = process.env.SIDEKICK_NODE_ENV === 'dev' ? 8011 : 8022;

class SidekickConnection {
  private socket: SocketIOClient.Socket;

  controlEmitter: EventEmitter = new EventEmitter();

  constructor(info?: any) {
    console.log('Initializing sidekick websocket connection');
    this.socket = io(`http://localhost:${terminalsPort}/terminal`, {
      transportOptions: {
        polling: {
          extraHeaders: {
            ...info,
          },
        },
      },
    });

    this.socket.on('connect_error', (error: any) => {
      console.log('Sidekick server connection error');
      console.error(error);
    });

    this.socket.on('connect_timeout', (error: any) => {
      console.error(error);
    });

    this.socket.on('awaiting_peer_session', (error: any) => {
      this.controlEmitter.emit('awaiting');
    });

    this.socket.on('update-entry', (callback: any) => {
      console.log('updating entry');
      callback(process.stdout.columns, process.stdout.rows)
    });

    this.socket.on('disconnect', () => {
      this.controlEmitter.emit('highlight_off');
      console.log('Disconnected from the sidekick server');
    });

    this.socket.on('join_peer_session', (sessionId: string) => {
      this.controlEmitter.emit('join_peer_session', sessionId);
    });

    this.socket.on('exit_peer_session', () => {
      this.controlEmitter.emit('exit_peer_session');
    });

    this.socket.on('create_peer_session', (sessionId: string) => {
      this.controlEmitter.emit('create_peer_session', sessionId);
    });

    this.socket.on('highlight_on', (message: string, color: string) => {
      console.log('highligh on');
      this.controlEmitter.emit('highlight_on', message, color);
    });

    this.socket.on('highlight_off', () => {
      this.controlEmitter.emit('highlight_off');
    });
  }

  async connect() {
    await new Promise((resolve, reject) => {
      this.socket.once('connect_error', () => {
        console.log('Connected to the sidekick server');
        return resolve();
      });
      this.socket.once('error', (error: any) => {
        console.log('Error when connecting to the sidekick server');
        return reject(error);
      });
      this.socket.connect();
    });
  }
}


export { SidekickConnection };
