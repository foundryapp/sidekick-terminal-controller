import io from 'socket.io-client';
import { EventEmitter } from 'events';

class WSConnection {
  private socket: SocketIOClient.Socket;

  signalEmitter: EventEmitter = new EventEmitter();
  controlEmitter: EventEmitter = new EventEmitter();

  waitForSessionMember = new Promise((resolve) => {
    this.resolveHook = resolve;
  });

  private resolveHook?: any;

  constructor(uri: string, private sessionId: string) {
    console.log('create ws for session', this.sessionId);
    this.socket = io(`${uri}/signalling`, {
      transportOptions: {
        polling: {
          extraHeaders: {
            session: sessionId,
          },
        },
      },
    });

    this.socket.on('connect_error', (error: any) => {
      console.log('Connection error - remote ws server');
      console.error(error);
    });

    this.socket.on('connect_timeout', (error: any) => {
      console.error(error);
    });

    this.socket.on('signal', (data: string) => {
      this.signalEmitter.emit('incoming_signal', data);
    });

    this.socket.on('joined_session', (sessionId: string, members: number) => {
      console.log('joined_session', sessionId, members);
      if (sessionId === this.sessionId && members === 2) {
        this.resolveHook?.();
      }
    });

    this.socket.on('left_session', () => {
      this.controlEmitter.emit('close');
    });

    this.signalEmitter.on('outgoing_signal', (data: string) => {
      if (this.socket.connected) {
        this.socket.emit('signal', data);
        return;
      }
      return Promise.reject('Socket is not connected to the server');
    });
  }

  disconnect() {
    this.socket.disconnect();
  }

  async connect() {
    await new Promise((resolve, reject) => {
      this.socket.once('connect', () => {
        return resolve();
      });
      this.socket.once('error', (error: any) => {
        return reject(error);
      });
      this.socket.connect();
    });
  }
}


export { WSConnection };
