import { WSConnection } from './webSocket';
import { WRTCConnection, TerminalData } from './webRTC';
import { EventEmitter } from 'events';

const webSocketUri = process.env.SIDEKICK_NODE_ENV === 'dev' ? 'http://localhost:80' : 'http://35.223.17.119';

class RemoteConnection {
  private ws: WSConnection;
  private wrtc?: WRTCConnection;

  dataEmitter = new EventEmitter();
  controlEmitter = new EventEmitter();


  constructor(sessionId: string) {
    this.ws = new WSConnection(webSocketUri, sessionId);
    this.ws.controlEmitter.on('close', () => {
      console.log('close remote connection event');
      this.controlEmitter.emit('close');
    });
  }

  async initialize() {
    return this.ws.connect();
  }

  private async createWRTC(initiator: boolean = false) {
    this.wrtc?.destroy();
    this.wrtc = new WRTCConnection(initiator, this.ws.signalEmitter);

    this.wrtc.dataEmitter.on('stdin', (data) =>
      this.dataEmitter.emit('stdin', data)
    );
    this.wrtc.dataEmitter.on('stdout', (data) =>
      this.dataEmitter.emit('stdout', data)
    );
    this.wrtc.dataEmitter.on('stderr', (data) =>
      this.dataEmitter.emit('stderr', data)
    );
    this.wrtc.dataEmitter.on('resize', (data) =>
      this.dataEmitter.emit('resize', data)
    );

    this.wrtc.controlEmitter.on('connect', () =>
      this.controlEmitter.emit('connect')
    );

    this.wrtc.controlEmitter.once('close', () => {
      console.log('close<<>>');
      this.controlEmitter.emit('close');
    });

    try {
      await this.wrtc.connect();
    } catch (error) {
      this.wrtc.destroy();
      console.error(error);
    } finally {
      // this.ws.disconnect();
    }
  }

  async createPeerSession() {
    console.log('create peer session');
    await this.ws.waitForSessionMember;
    console.log('awaited member');
    await this.createWRTC(true);
    console.log('created wrct');
  }

  close() {
    this.ws.disconnect();
    this.wrtc?.destroy();
  }

  async joinPeerSession() {
    console.log('join peer session');
    await this.createWRTC();
    console.log('connected');
  }

  sendData(type: TerminalData, data: string) {
    if (this.wrtc) {
      this.wrtc.sendData(type, data);
      return;
    }
    // console.log('Wrtc not conncted');
  }
}


export { RemoteConnection };
