import Peer from 'simple-peer';
import { EventEmitter } from 'events';
import wrtc from 'wrtc';


type TerminalData = 'stdin' | 'stdout' | 'stderr' | 'resize';

const iceServers = [
  // TODO: Add our own STUN and TURN server - https://github.com/gortc/gortcd
  // { urls: 'stun:localhost:3478' },
  // { urls: 'turn:localhost:3478' },
  { urls: 'stun:stun.l.google.com:19302' },
  { urls: 'stun:stun1.l.google.com:19302' },
  { urls: 'stun:stun2.l.google.com:19302' },
  { urls: 'stun:stun3.l.google.com:19302' },
  { urls: 'stun:stun4.l.google.com:19302' },
];

class WRTCConnection {
  private peer?: Peer.Instance;

  dataEmitter: EventEmitter = new EventEmitter();
  controlEmitter: EventEmitter = new EventEmitter();

  isConnected = false;

  constructor(private initiator: boolean, private signalEmitter: EventEmitter) {
    if (!this.initiator) {
      this.initialize();
    }
  }

  sendData(type: TerminalData, data: any) {
    if (this.peer && !this.peer.destroyed && this.isConnected) {
      this.peer?.send(JSON.stringify({ type, data }));
      return;
    }
    console.log('Peer is not connected.');
  }

  private initialize() {
    this.peer = new Peer({
      initiator: this.initiator,
      wrtc,
      reconnectTimer: 0,
      config: {
        iceServers,
      },
    });

    console.log('initializing wrtc');

    this.signalEmitter.on('incoming_signal', (data) => {
      this.signalListener(data);
    });

    this.peer.on('signal', (data: any) => {
      this.signalEmitter.emit('outgoing_signal', data);
    });

    this.peer.on('data', (rawData: any) => {
      const data = JSON.parse(rawData) as { type: TerminalData, data: any };
      this.dataEmitter.emit(data.type, data.data);
    });

    this.peer.on('error', (error: any) => {
      this.isConnected = false;
      console.error(error);
    });

    this.peer.on('connect', () => {
      this.isConnected = true;
      this.controlEmitter.emit('connect');
    });
    this.peer.on('close', () => {
      this.isConnected = false;
      console.log('closed connection');
      this.controlEmitter.emit('close');
    });
  }

  async connect() {
    if (this.initiator) {
      this.initialize();
    }
    const connecting = new Promise((resolve, reject) => {
      const resolver = () => {
        if (this.peer) {
          this.peer.removeListener('error', rejecter);
          return resolve();
        } else {
          return reject('Peer does not exist.');
        }
      }

      const rejecter = (error: any) => {
        if (this.peer) {
          this.peer.removeListener('connect', resolver);
          return reject(error);
        } else {
          return reject('Peer does not exist.')
        }
      }

      if (this.peer) {
        this.peer.once('connect', resolver);
        this.peer.once('error', rejecter);
      } else {
        return reject('Cannot add listeners - Peer does not exist.')
      }
    });

    return connecting;
  }

  private signalListener(data: any) {
    if (this.peer && !this.peer.destroyed) {
      this.peer.signal(data);
      return;
    }
    console.log('Peer is not connected.');
  }

  destroy() {
    if (this.peer && !this.peer.destroyed) {
      this.signalEmitter.removeListener('signal', this.signalListener.bind(this))
      this.peer.destroy();
      return;
    }
    console.log('Peer does not exist or was already destroyed.');
  }
}


export { WRTCConnection, TerminalData };
