#!/usr/bin/env node

// this line must be before importing session
process.env.SIDEKICK_NODE_ENV = process.argv[2] === 'dev' ? 'dev' : 'prod';
const logs = process.argv[3] === 'logs'

import { Session } from './session';


if (!logs) {
  console.log = () => { };
  console.error = () => { };
}

console.log('SIDEKICK_NODE_ENV', process.env.SIDEKICK_NODE_ENV);

async function main(forkCount: number) {
  const session = new Session(forkCount);
  await session.connect();
}

console.log('SIDEKICK_FORK_COUNT', process.env.SIDEKICK_FORK_COUNT);

const forkCount = parseInt(process.env.SIDEKICK_FORK_COUNT || '1');

if (forkCount % 2 !== 0) {
  main(forkCount + 1);
}
