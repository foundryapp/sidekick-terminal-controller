import os from 'os';
import * as pty from 'node-pty';
import { Highlight } from './highlight';


const defaultShell = process.env.SHELL || (os.platform() === 'win32' ? 'powershell.exe' : 'bash');


class Terminal {
  private highlight?: Highlight;
  private terminal: pty.IPty;

  get onData() {
    return this.terminal.onData;
  }

  get pid() {
    return this.terminal.pid;
  }


  get cols() {
    return process.stdout.columns;
  }

  get rows() {
    return process.stdout.rows;
  }

  constructor(forkCount: number) {
    const { cols, rows } = this.getTTYSize();

    this.terminal = pty.spawn(defaultShell, [], {
      rows,
      cols,
      cwd: process.env.PWD,
      env: {
        ...process.env as { [key: string]: string },
        TERM: 'xterm-256color',
        SIDEKICK_FORK_COUNT: forkCount.toString(),
      },
    });

    process.stdin.setRawMode(true);


    // process.stdout.write('Started sidekick terminal session\n');

    this.terminal.onExit(() => {
      process.stdout.write('Exited sidekick terminal session\n');
      process.exit();
    });

    // this.terminal.write('echo -------------->>');
  }

  getTTYSize() {
    const rows = process.stdout.rows;
    const cols = process.stdout.columns;
    return {
      rows,
      cols,
    };
  }

  turnHighlightOff() {
    this.highlight?.remove();
    this.highlight = undefined;
    this.refresh();
  }

  turnHighlightOn(message: string, color: string) {
    this.highlight?.remove();
    this.highlight = new Highlight(message, color);
    this.highlight.render();
  }

  setSize(cols: number, rows: number) {
    this.terminal.resize(cols, rows);
  }

  adjustSize() {
    const { cols, rows } = this.getTTYSize();
    this.highlight?.remove();
    this.setSize(cols, rows);
    this.refresh();
    this.highlight?.render();
  }

  refresh() {
    const { cols, rows } = this.getTTYSize();
    this.setSize(cols + 1, rows);
    this.setSize(cols, rows);
  }

  write(data: string) {
    this.terminal.write(data);
  }
}


export { Terminal };
