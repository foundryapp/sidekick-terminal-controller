import { terminal as screenControl } from 'terminal-kit';
import boxen, { BorderStyle } from 'boxen';

class Highlight {
  private box: string;
  private whiteBox: string;

  private width: number;
  private height: number;

  private startX?: number;
  private startY?: number;

  constructor(message: string, borderColor: string) {
    const boxOptions = {
      padding: 1,
      borderStyle: BorderStyle.Double,
    };
    this.whiteBox = boxen(message, boxOptions);
    this.box = boxen(message, {
      ...boxOptions,
      borderColor,
    });

    this.width = this.whiteBox.split('\n').map((line) => line.length).reduce((max, current) => {
      return Math.max(max, current);
    });
    this.height = this.whiteBox.split('\n').length;
  }

  render() {
    console.log('highligh rerender');
    const cols = process.stdout.columns;
    const rows = process.stdout.rows;

    const startX = Math.round((cols - this.width) / 2);
    const startY = Math.round((rows - this.height) / 2);

    this.startX = startX;
    this.startY = startY;

    this.remove();

    this.startX = startX;
    this.startY = startY;

    screenControl.saveCursor();
    this.box.split('\n').forEach((line, index) => {
      screenControl.moveTo(this.startX, this.startY + index, line);
    });
    screenControl.restoreCursor();
  }

  remove() {
    if (this.startX !== undefined && this.startY !== undefined) {
      screenControl.saveCursor();
      screenControl.eraseArea(this.startX, this.startY, this.width, this.height);
      screenControl.restoreCursor();

      this.startX = undefined;
      this.startY = undefined;
    }
  }
}

export { Highlight };